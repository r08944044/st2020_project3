# -*- coding: utf-8 -*
import os
import time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_1():
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('查看商品分類') != -1
    assert xmlString.find('查訂單/退訂退款') != -1
    assert xmlString.find('追蹤/買過/看過清單') != -1
    assert xmlString.find('智慧標籤') != -1
    assert xmlString.find('其他') != -1
    assert xmlString.find('PChome 旅遊') != -1
    assert xmlString.find('線上手機回收') != -1
    assert xmlString.find('給24h購物APP評分') != -1

# 2. [Screenshot] Side Bar Text
def test_2():
    os.system('adb shell screencap -p /sdcard/02_side_bar_text.png')
    os.system('adb pull /sdcard/02_side_bar_text.png')

# 3. [Context] Categories
def test_3():
    os.system('adb shell input tap 1000 100')
    os.system('adb shell input swipe 500 1500 500 500')
    os.system('adb shell input tap 1000 300')
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('精選') != -1
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1

# 4. [Screenshot] Categories
def test_4():
    os.system('adb shell screencap -p /sdcard/04_categories.png')
    os.system('adb pull /sdcard/04_categories.png')

# 5. [Context] Categories page
def test_5():
    os.system('adb shell input tap 500 1000')
    time.sleep(1)
    os.system('adb shell input tap 300 1700')
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1

# 6. [Screenshot] Categories page
def test_6():
    os.system('adb shell screencap -p /sdcard/06_categories_page.png')
    os.system('adb pull /sdcard/06_categories_page.png')

# 7. [Behavior] Search item “switch”
def test_7():
    os.system('adb shell input tap 300 150')
    os.system('adb shell input text "switch"')
    os.system('adb shell input keyevent "KEYCODE_ENTER"')
    time.sleep(8)
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('switch') or xmlString.find('Switch') != -1

# 8. [Behavior] Follow an item and it should be add to the list
def test_8():
    os.system('adb shell input tap 500 500')
    time.sleep(8)
    os.system('adb shell input tap 100 1700')
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('追') != -1
    assert xmlString.find('蹤') != -1
    
    time.sleep(4)
    os.system('adb shell input tap 100 100')
    time.sleep(4)
    os.system('adb shell input tap 100 1700')
    time.sleep(4)
    os.system('adb shell input tap 100 100')
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('追蹤/買過/看過清單') != -1
    
    time.sleep(4)
    os.system('adb shell input tap 300 900')
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('switch') or xmlString.find('Switch') != -1

# 9. [Behavior] Navigate to the detail of item
def test_9():
    time.sleep(8)
    os.system('adb shell input tap 300 900')
    time.sleep(8)
    os.system('adb shell input swipe 500 1500 500 500')
    os.system('adb shell input tap 550 150')
    os.system('adb shell uiautomator dump')
    os.system('adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('詳') != -1
    assert xmlString.find('情') != -1

# 10. [Screenshot] Disconnetion Screen
def test_10():
    time.sleep(4)
    os.system('adb shell svc data disable')
    os.system('adb shell svc wifi disable')
    time.sleep(1)
    os.system('adb shell screencap -p /sdcard/10_disconnetion_screen.png')
    os.system('adb pull /sdcard/10_disconnetion_screen.png')
    
def test_recover():
    time.sleep(4)
    os.system('adb shell svc data enable')
    os.system('adb shell svc wifi enable')
    os.system('adb shell input tap 100 1700')
